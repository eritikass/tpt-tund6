it('1 + 1 => 2', () => {
    expect(1 + 1).toBe(2);
});

it('3 - 1 => 2', () => {
    expect(3 - 1).toBe(2);
});

it('2 - 1 => 1', () => {
    expect(2 - 1).toBe(1);
});

it('2 - 2 => 0', () => {
    expect(2 - 2).toBe(0);
});